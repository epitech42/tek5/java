// This is just an example,
// so you can safely delete all default props below

export default {
  failed: 'Action failed',
    success: 'Action was successful',
    Feed: {
	"title": "Title",
	"description": "Description",
	"author": "Author",
	"goToNews": "Go to News",
	"copyright": "Copyrights",
	"language": "Language"
    },
    Form: {
	"addFeedInput": "Add Feed URL",
	"username": "name",
	"password": "password",
	"FeedName": "Feed Name",
	"FeedURL": "Feed URL",
    }
}
