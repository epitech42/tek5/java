
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
	{ path: '', component: () => import('pages/Index.vue') },
	{ path: 'addFeed', component: () => import('pages/AddFeed.vue') },
	{ path: 'getFeeds', component: () => import('pages/GetFeeds.vue') },
	{ path: 'SignUp', component: () => import('pages/SignUp.vue') },
	{ path: 'LogIn', component: () => import('pages/LogIn.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
