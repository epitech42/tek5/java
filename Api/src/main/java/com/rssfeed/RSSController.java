package com.rssfeed;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@CrossOrigin(origins="*", allowedHeaders = "*")
@RequestMapping("api/rss")
public class RSSController {
    @Autowired
    private RSSService rssService;

    @RequestMapping(value = { "/getFeeds" }, method = RequestMethod.GET, produces = "application/json")
    public Object getFeeds(HttpServletRequest req) {
        try {
            return new ResponseEntity<String>(rssService.getSubscriptions(req), HttpStatus.OK);
        } catch (CustomException e) {
            e.printStackTrace();
            return new ResponseEntity<String>("Error: " + e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = { "/addFeed" }, method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public ResponseEntity<String> addFeed(@RequestBody Subscription subscription, HttpServletRequest req) {
        try {
            rssService.addSubscription(subscription.getName(), subscription.getUrl(), req);
            return new ResponseEntity<String>("Feed " + subscription.getName() + " added", HttpStatus.OK);
        } catch (CustomException e) {
            e.printStackTrace();
            return new ResponseEntity<String>("Error: " + e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}


