
package com.rssfeed;

import org.springframework.data.repository.CrudRepository;
import java.util.ArrayList;

public interface SubscriptionRepository extends CrudRepository<Subscription, Integer> {
    public ArrayList<Subscription> findByUser_Id(int id);
    public Subscription findByName(String name);
    public Subscription findById(int id);
    public long count();
}
