package com.rssfeed;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;

@Service
public class RSSService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SubscriptionRepository subscriptionRepository;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    public void addSubscription(String name, String url, HttpServletRequest req) throws CustomException {
        if (name == null || name.isEmpty()) {
            throw new CustomException("Name cannot be empty");
        }
        if (url == null || url.isEmpty()) {
            throw new CustomException("Url cannot be empty");
        }
        Subscription subscription = new Subscription();
        subscription.setName(name);
        subscription.setUrl(url);
        subscription.setUser(userRepository.findById(jwtTokenProvider.getId(jwtTokenProvider.resolveToken(req))));
        subscription.setLastReadingDate(new Date());
        subscriptionRepository.save(subscription);
    }

    public String getSubscriptions(HttpServletRequest req) throws CustomException {
        ArrayList<Subscription> subscriptions = subscriptionRepository.findByUser_Id(jwtTokenProvider.getId(jwtTokenProvider.resolveToken(req)));
        ArrayList<Feed> resultList = new ArrayList<Feed>();
        for (Subscription subscription: subscriptions) {
            RSSFeedParser parser = new RSSFeedParser(subscription.getUrl());
            resultList.add(parser.readFeed(subscription.getId(), subscription.getName(), subscription.getUrl()));
        }
        return new Gson().toJson(resultList);
    }
}
