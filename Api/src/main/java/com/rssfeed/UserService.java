package com.rssfeed;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.rssfeed.UserRepository;
import com.rssfeed.User;
    
@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    public static String encryptThisString(String input) throws RuntimeException
    {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            byte[] messageDigest = md.digest(input.getBytes());
            BigInteger no = new BigInteger(1, messageDigest);
            String hashtext = no.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        }
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public void registerNewUserAccount(String name, String password) throws CustomException{
            if (userRepository.findByName(name) != null) {
                throw new CustomException("User " + name + " already exist");
            }
            if (name == null || name.isEmpty()) {
                throw new CustomException("Name cannot be empty");
            }
            if (password == null || password.isEmpty()) {
                throw new CustomException("Password cannot be empty");
            }
            User user = new User();
            user.setName(name);
            user.setPassword(encryptThisString(password));
            userRepository.save(user);
    }

    public String connection(String name, String password) throws CustomException{
        if (name == null || name.isEmpty()) {
            throw new CustomException("Name cannot be empty");
        }
        if (password == null || password.isEmpty()) {
            throw new CustomException("Password cannot be empty");
        }
        String hash = encryptThisString(password);
        User user = userRepository.findByName(name);
        if (user == null) {
            throw new CustomException("User " + name + " not found");
        }
        if (hash.equals(user.getPassword())) {
            return jwtTokenProvider.createToken(name, user.getId());
        }
        else {
            throw new CustomException("Wrong password");
    	}
    }
}
