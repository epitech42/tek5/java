package com.rssfeed;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import org.springframework.beans.factory.annotation.Autowired;

@RestController
@CrossOrigin(origins="*", allowedHeaders = "*")
@RequestMapping("api/user")
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    //@PostMapping("/signup")
    @RequestMapping(value = { "/signup" }, method = RequestMethod.POST, produces = "text/plain", consumes = "application/json")
    public ResponseEntity<String> signup(@RequestBody User user){//@RequestParam String name, @RequestParam String password) {
        try {
            userService.registerNewUserAccount(user.getName(), user.getPassword());
        } catch (CustomException e) {
            e.printStackTrace();
            return new ResponseEntity<String>("Error: " + e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<String>("User " + user.getName() + " added", HttpStatus.OK);
    }

    //@PostMapping("/login")
    @RequestMapping(value = { "/login" }, method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public ResponseEntity<String> login(@RequestBody User user){//@RequestParam String name, @RequestParam String password) {
        try {
            String token = userService.connection(user.getName(), user.getPassword());
            return new ResponseEntity<String>(token, HttpStatus.OK);
        } catch (CustomException e) {
            e.printStackTrace();
            return new ResponseEntity<String>("Error: " + e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
