package com.rssfeed;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

//@Component("userRepository")
public interface UserRepository extends CrudRepository<User, Integer> {
    public User findByName(String name);
    public User findById(int id);
    public long count();
}
