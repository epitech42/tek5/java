package com.rssfeed;

public class CustomException extends RuntimeException {
    public CustomException() {
        super();
    }

    public CustomException(final String message) {
        super(message);
    }
}
