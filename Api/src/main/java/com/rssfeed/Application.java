package com.rssfeed;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@CrossOrigin(origins="*", allowedHeaders = "*")
@RestController
public class Application {

    @RequestMapping("/")
    public String home() {
        return "Hello Docker World";
    }

    @RequestMapping(value= "api/**", method=RequestMethod.OPTIONS)
    public ResponseEntity<String> corsHeaders(HttpServletResponse response) {
	response.addHeader("Access-Control-Allow-Origin", "*");
	response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
	response.addHeader("Access-Control-Allow-Headers", "origin, content-type, accept, x-requested-with");
	response.addHeader("Access-Control-Max-Age", "3600");
	return new ResponseEntity<String>("tkt", HttpStatus.OK);
    }
    
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
