# Java

## Prerequisites

JDK (**>= 8**): https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html

Maven (**>= 3.6.3**): http://maven.apache.org/download.cgi

## Quick start

First, clone the project.

Once done, move to the `/Api` directory of this project and run:

```
mvn clean compile package
```

Lastly, you can run the jar file with:

```
java -jar target/RSSFeedProject-{version}.jar
```

Now go to http://localhost:8080/api/rss/getFeed and see the RSS Feed from lemonde.fr/cultures-web/

