package eu.epitech.RSSFeed;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;

import static eu.epitech.RSSFeed.FeedFragment.EXTRA_CREATOR;
import static eu.epitech.RSSFeed.FeedFragment.EXTRA_DATE;
import static eu.epitech.RSSFeed.FeedFragment.EXTRA_DESC;
import static eu.epitech.RSSFeed.FeedFragment.EXTRA_LINK;
import static eu.epitech.RSSFeed.FeedFragment.EXTRA_TITLE;

public class PostActivity extends AppCompatActivity {
    String mLink = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);
        Intent intent = getIntent();
        String title = intent.getStringExtra(EXTRA_TITLE);
        String desc = intent.getStringExtra(EXTRA_DESC);
        mLink = intent.getStringExtra(EXTRA_LINK);
        String author = intent.getStringExtra(EXTRA_CREATOR);
        String date = intent.getStringExtra(EXTRA_DATE);

        TextView textViewTitle = findViewById(R.id.title_post_view);
        TextView textViewDesc = findViewById(R.id.desc_post_view);
        TextView textViewAuthor = findViewById(R.id.author_post_view);
        TextView textViewDate = findViewById(R.id.pub_date_post_view);
        TextView textViewLink = findViewById(R.id.link_post_view);

        textViewTitle.setText(title);
        textViewDesc.setText(desc);
        textViewAuthor.setText(author);
        textViewDate.setText(date);
        textViewDate.setText(date);
        textViewLink.setText(mLink);

        Button buttonLink = findViewById(R.id.button_link_post_view);
        buttonLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mLink.startsWith("https://") || mLink.startsWith("http://")) {
                    Uri uri = Uri.parse(mLink);
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                } else {
                    Toast.makeText(PostActivity.this, "Invalid Url", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
