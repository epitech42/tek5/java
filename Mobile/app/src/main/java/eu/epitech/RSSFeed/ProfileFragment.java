package eu.epitech.RSSFeed;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ProfileFragment extends Fragment {
    private RequestQueue mRequestQueue;
    private Context mContext;
    private String mToken;
    private TextView mFeeds;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        mContext = view.getContext();
        mToken = getActivity().getIntent().getStringExtra("TOKEN");
        TextView username = (TextView) view.findViewById(R.id.text_profile);
        username.setText(getActivity().getIntent().getStringExtra("USERNAME"));

        mRequestQueue = Volley.newRequestQueue(mContext);
        parseJSON(Config.MainURL + "/rss/getFeeds");
        mFeeds = (TextView) view.findViewById(R.id.text_nb_links);
        return (view);
    }

    private void parseJSON(String url) {
        final JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        int j = 0;
                        while (j < response.length()) { j++; }
                        String tmp = "Followed links : " + Integer.toString(j);
                        mFeeds.setText(tmp);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Cant connect to backend." + error, Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + mToken);
                return headers;
            }
        };
        mRequestQueue.add(request);
    }
}
