package eu.epitech.RSSFeed;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;
import java.nio.charset.StandardCharsets;

public class RegisterFragment extends Fragment {
    private String username = "";
    private String password = "";
    private String repassword = "";
    private RequestQueue mRequestQueue = null;
    private Context mContext;

    public RegisterFragment() {
        // Required empty public constructor
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register, container, false);
        mContext = view.getContext();
        // Inflate the layout for this fragment
        EditText mEditTextLogin = view.findViewById(R.id.et_name);
        mEditTextLogin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                username = s.toString();
            }
        });
        EditText mEditTextPass = view.findViewById(R.id.et_password);
        mEditTextPass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                password = s.toString();
            }
        });
        EditText mEditTextRePass = view.findViewById(R.id.et_repassword);
        mEditTextRePass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                repassword = s.toString();
            }
        });
        mRequestQueue = Volley.newRequestQueue(mContext);

        Button button = (Button) view.findViewById(R.id.btn_register);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Register(v);
            }
        });
        return view;
    }

    public void Register(View v) {
        if (!(username.equals("")) && !(password.equals("")) && !(repassword.equals(""))) {
            if (!password.equals(repassword))
                Toast.makeText(getContext(), "Password doesn't match", Toast.LENGTH_SHORT).show();
                else
            try {
                JSONObject jsonBody = new JSONObject();
                jsonBody.put("name", username);
                jsonBody.put("password", password);
                final String requestBody = jsonBody.toString();
                StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.MainURL + "/user/signup", new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(getContext(), response, Toast.LENGTH_SHORT).show();
                    }
                },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(getContext(), "Cant connect to backend. " + error, Toast.LENGTH_LONG).show();
                            }
                        }) {
                    @Override
                    public String getBodyContentType() {
                        return "application/json; charset=utf-8";
                    }
                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        return requestBody.getBytes(StandardCharsets.UTF_8);
                    }
                };
                mRequestQueue.add(stringRequest);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else
            Toast.makeText(getContext(), "Fill username and password", Toast.LENGTH_SHORT).show();
    }
}