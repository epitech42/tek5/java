package eu.epitech.RSSFeed;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;

public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.FeedViewHolder> {
    private Context mContext;
    private ArrayList<ItemCardView> mFeedList;
    private OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public FeedAdapter(Context context, ArrayList<ItemCardView> feedList) {
        mContext = context;
        mFeedList = feedList;
    }

    @Override
    public FeedViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.item_cardview, parent, false);
        return new FeedViewHolder(v);
    }

    @Override
    public void onBindViewHolder(FeedViewHolder holder, int position) {
        ItemCardView currentItem = mFeedList.get(position);
        String titleName = currentItem.getTitle();
        String desc = currentItem.getDescription();
        String author = currentItem.getAuthor();
        String date = currentItem.getDate();

        holder.mTextViewTitle.setText(titleName);
        holder.mTextViewDesc.setText(desc);
        holder.mTextViewAuthor.setText(author);
        holder.mTextViewDate.setText(date);
    }

    @Override
    public int getItemCount() {
        return mFeedList.size();
    }

    public class FeedViewHolder extends RecyclerView.ViewHolder {
        public TextView mTextViewTitle;
        public TextView mTextViewDesc;
        public TextView mTextViewAuthor;
        public TextView mTextViewDate;

        public FeedViewHolder(View itemView) {
            super(itemView);
            mTextViewTitle = itemView.findViewById(R.id.title_feed_view);
            mTextViewDesc = itemView.findViewById(R.id.desc_feed_view);
            mTextViewAuthor = itemView.findViewById(R.id.author_feed_view);
            mTextViewDate = itemView.findViewById(R.id.pub_date_feed_view);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION)
                            mListener.onItemClick(position);
                    }
                }
            });
        }
    }
}
