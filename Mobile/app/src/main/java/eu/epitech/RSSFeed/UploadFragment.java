package eu.epitech.RSSFeed;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class UploadFragment extends Fragment {
    private String access_token = null;
    private String title = null;
    private String url = null;
    private RequestQueue mRequestQueue = null;
    private Context mContext = null;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_upload, container, false);
        mContext = view.getContext();

        access_token = getActivity().getIntent().getStringExtra("TOKEN");

        Button addFeed = view.findViewById(R.id.add_feed);
        EditText editTextTitle = view.findViewById(R.id.title_edit_text);
        editTextTitle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                title = s.toString();
            }
        });
        EditText editText = view.findViewById(R.id.link_upload_edit_text);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                url = s.toString();
            }
        });
        addFeed.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mRequestQueue = Volley.newRequestQueue(mContext);
                sendRequest();
            }
        });
        return view;
    }

    public void sendRequest() {
        if (url != null && title != null) {
            if ((title.length() > 0 && url.length() > 0) && (access_token != null && access_token.length() > 0)) {
                JSONObject jsonBody = new JSONObject();
                try {
                    jsonBody.put("name", title);
                    jsonBody.put("url", url);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                final String requestBody = jsonBody.toString();
                StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.MainURL + "/rss/addFeed", new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response.endsWith("added")) {
                            ImageView uploadState = getActivity().findViewById(R.id.uploaded);
                            uploadState.setVisibility(View.VISIBLE);
                            getActivity().findViewById(R.id.fail_upload).setVisibility(View.INVISIBLE);
                            Toast.makeText(getContext(), title + " has been successfully added.", Toast.LENGTH_LONG).show();
                        } else {
                            ImageView uploadState = getActivity().findViewById(R.id.fail_upload);
                            uploadState.setVisibility(View.VISIBLE);
                            getActivity().findViewById(R.id.uploaded).setVisibility(View.INVISIBLE);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(), "Cant connect to backend. " + error, Toast.LENGTH_LONG).show();
                    }
                }) {
                    @Override
                    public String getBodyContentType() {
                        return "application/json; charset=utf-8";
                    }
                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        return requestBody.getBytes(StandardCharsets.UTF_8);
                    }
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> headers = new HashMap<>();
                        headers.put("Authorization", "Bearer " + access_token);
                        return headers;
                    }
                };
                mRequestQueue.add(stringRequest);
            } else
                Toast.makeText(mContext, "URL Invalide", Toast.LENGTH_SHORT).show();
        } else
            Toast.makeText(mContext, "URL Invalide", Toast.LENGTH_SHORT).show();
    }
}
