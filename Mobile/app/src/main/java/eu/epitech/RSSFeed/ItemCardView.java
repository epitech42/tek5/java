package eu.epitech.RSSFeed;

public class ItemCardView {
    private String mTitle;
    private String mDescription;
    private String mAuthor;
    private String mDate;
    private String mLink;

    public ItemCardView(String title, String description, String author, String date, String link) {
        mTitle = title;
        mDescription = description;
        mAuthor = author;
        mDate = date;
        mLink = link;
    }

    public String getTitle() {
        return (mTitle);
    }
    public String getDescription() {
        return (mDescription);
    }
    public String getAuthor() {
        return (mAuthor);
    }
    public String getDate() {
        return (mDate);
    }
    public String getLink() {
        return (mLink);
    }
}
