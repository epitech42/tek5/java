package eu.epitech.RSSFeed;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static eu.epitech.RSSFeed.FeedFragment.EXTRA_CREATOR;
import static eu.epitech.RSSFeed.FeedFragment.EXTRA_TITLE;
import static eu.epitech.RSSFeed.FeedFragment.EXTRA_DESC;
import static eu.epitech.RSSFeed.FeedFragment.EXTRA_DATE;
import static eu.epitech.RSSFeed.FeedFragment.EXTRA_LINK;

public class SearchFragment extends Fragment implements FeedAdapter.OnItemClickListener {
    private RecyclerView mRecyclerView;
    private FeedAdapter mFeedAdapter;
    private ArrayList<ItemCardView> mFeedList;
    private RequestQueue mRequestQueue;
    private Context mContext;
    private String mToken;
    private String mCategory;
    private String mSearch = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        mContext = view.getContext();
        mToken = getActivity().getIntent().getStringExtra("TOKEN");

        final Spinner spinnerCategory = (Spinner) view.findViewById(R.id.search_spinner);
        String[] lCat = { "All Categories", "Title", "Content", "Source" };
        ArrayAdapter<String> dataAdapterC = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, lCat);
        dataAdapterC.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCategory.setAdapter(dataAdapterC);

        spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                mCategory = String.valueOf(spinnerCategory.getSelectedItem());
                mFeedList = new ArrayList<>();
                mRequestQueue = Volley.newRequestQueue(mContext);
                parseJSON(mSearch);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        mRecyclerView = view.findViewById(R.id.search_recycler_view);
        EditText editText = view.findViewById(R.id.search_edit_text);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                mRecyclerView.setHasFixedSize(true);
                mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
                mSearch = s.toString();
                mFeedList = new ArrayList<>();
                mRequestQueue = Volley.newRequestQueue(mContext);
                parseJSON(mSearch);
            }
        });
        return (view);
    }

    private void parseJSON(final String search) {
        final JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, Config.MainURL + "/rss/getFeeds", null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            for (int j = 0; j < response.length(); j++) {
                                JSONArray jsonArray = response.getJSONObject(j).getJSONArray("entries");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject data = jsonArray.getJSONObject(i);
                                    String title = data.getString("title");
                                    String desc = data.getString("description");
                                    String link = data.getString("link");
                                    String author = data.getString("author");
                                    String date = data.getString("pubDate");
                                    String[] lCat = {"All Categories", "Title", "Content", "Source"};
                                    if (search.equals(""))
                                        mFeedList.add(new ItemCardView(title, desc, author, date, link));
                                    else if (mCategory.equals(lCat[0])) {
                                        if (title.contains(search) || desc.contains(search) || link.contains(search) || author.contains(search))
                                            mFeedList.add(new ItemCardView(title, desc, author, date, link));
                                    } else if (mCategory.equals(lCat[1])) {
                                        if (title.contains(search))
                                            mFeedList.add(new ItemCardView(title, desc, author, date, link));
                                    } else if (mCategory.equals(lCat[2])) {
                                        if (desc.contains(search))
                                            mFeedList.add(new ItemCardView(title, desc, author, date, link));
                                    } else if (mCategory.equals(lCat[3])) {
                                        if (link.contains(search) || author.contains(search))
                                            mFeedList.add(new ItemCardView(title, desc, author, date, link));
                                    }
                                }
                                mFeedAdapter = new FeedAdapter(mContext, mFeedList);
                                mRecyclerView.setAdapter(mFeedAdapter);
                                mFeedAdapter.setOnItemClickListener(SearchFragment.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mContext, "Cant connect to backend." + error, Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + mToken);
                return headers;
            }
        };
        mRequestQueue.add(request);
    }

    @Override
    public void onItemClick(int position) {
        Intent detailIntent = new Intent(mContext, PostActivity.class);
        ItemCardView clickedItem = mFeedList.get(position);

        detailIntent.putExtra(EXTRA_TITLE, clickedItem.getTitle());
        detailIntent.putExtra(EXTRA_DESC, clickedItem.getDescription());
        detailIntent.putExtra(EXTRA_CREATOR, clickedItem.getAuthor());
        detailIntent.putExtra(EXTRA_DATE, clickedItem.getDate());
        detailIntent.putExtra(EXTRA_LINK, clickedItem.getLink());

        startActivity(detailIntent);
    }
}
