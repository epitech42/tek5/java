package eu.epitech.RSSFeed;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static eu.epitech.RSSFeed.FeedFragment.EXTRA_CREATOR;
import static eu.epitech.RSSFeed.FeedFragment.EXTRA_TITLE;

public class UploadListFragment extends Fragment implements FeedAdapter.OnItemClickListener {
    private RecyclerView mRecyclerView;
    private FeedAdapter mFeedAdapter;
    private ArrayList<ItemCardView> mFeedList;
    private RequestQueue mRequestQueue;
    private Context mContext;
    private String mToken;
    public static final String EXTRA_TITLE = "title";
    public static final String EXTRA_DESC = "description";
    public static final String EXTRA_CREATOR = "authorName";
    public static final String EXTRA_DATE = "date";
    public static final String EXTRA_LINK = "link";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feed, container, false);
        mContext = view.getContext();
        mToken = getActivity().getIntent().getStringExtra("TOKEN");
        mRecyclerView = view.findViewById(R.id.feed_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));

        mFeedList = new ArrayList<>();
        mRequestQueue = Volley.newRequestQueue(mContext);
        parseJSON(Config.MainURL + "/rss/getFeeds");
        return (view);
    }

    private void parseJSON(String url) {
        final JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            for (int j = 0; j < response.length(); j++) {
                                JSONObject data = response.getJSONObject(j);
                                String title = data.getString("title");
                                String desc = data.getString("description");
                                String link = data.getString("link");
                                String name = data.getString("name");
                                String date = data.getString("pubDate");
                                mFeedList.add(new ItemCardView(name, desc, title, date, link));
                            }
                            mFeedAdapter = new FeedAdapter(mContext, mFeedList);
                            mRecyclerView.setAdapter(mFeedAdapter);
                            mFeedAdapter.setOnItemClickListener(UploadListFragment.this);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Cant connect to backend." + error, Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + mToken);
                return headers;
            }
        };
        mRequestQueue.add(request);
    }

    @Override
    public void onItemClick(int position) {
        Intent detailIntent = new Intent(mContext, PostActivity.class);
        ItemCardView clickedItem = mFeedList.get(position);

        detailIntent.putExtra(EXTRA_TITLE, clickedItem.getTitle());
        detailIntent.putExtra(EXTRA_DESC, clickedItem.getDescription());
        detailIntent.putExtra(EXTRA_CREATOR, clickedItem.getAuthor());
        detailIntent.putExtra(EXTRA_DATE, clickedItem.getDate());
        detailIntent.putExtra(EXTRA_LINK, clickedItem.getLink());

        startActivity(detailIntent);
    }
}
